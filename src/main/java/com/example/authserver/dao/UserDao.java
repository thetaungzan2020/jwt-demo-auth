package com.example.authserver.dao;

import com.example.authserver.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends CrudRepository<User,String> {

    Optional<User> findUserByUsername(String username);
}

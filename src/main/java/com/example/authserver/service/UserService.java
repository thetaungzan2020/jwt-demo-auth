package com.example.authserver.service;

import com.example.authserver.dao.OtpDao;
import com.example.authserver.dao.UserDao;
import com.example.authserver.entity.Otp;
import com.example.authserver.entity.User;
import com.example.authserver.utils.GenerateCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserDao userDao;
    private final OtpDao otpDao;

    public UserService(PasswordEncoder passwordEncoder, UserDao userDao, OtpDao otpDao) {
        this.passwordEncoder = passwordEncoder;
        this.userDao = userDao;
        this.otpDao = otpDao;
    }


    public void addUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    public void auth(User user) {
        Optional<User> optionalUser = userDao.findUserByUsername(user.getUsername());
        if(optionalUser.isPresent()) {
            User u = optionalUser.get();
            if(passwordEncoder.matches(u.getPassword(),user.getPassword())){
                renewOtp(u);
            }else {
                throw new BadCredentialsException("Bad Credentials");
            }
        }else {
            throw new BadCredentialsException("Bad Credentials");
        }
    }

    public boolean check(Otp otp) {
        Optional<Otp> userOtp = otpDao.findOtpByUsername(otp.getUsername());
        if(userOtp.isPresent()) {
            Otp otpUser = userOtp.get();
            if(otp.getCode().equals(otpUser.getCode())) {
                return true;
            }
        }
        return false;
    }

    private void renewOtp(User user) {
        String code= GenerateCodeUtil.generateCode();

        Optional<Otp> userOtp = otpDao.findOtpByUsername(user.getUsername());

        if(userOtp.isPresent()) {
            Otp otp = userOtp.get();
            otp.setCode(code);
        }else {
            Otp otp=new Otp();
            otp.setUsername(user.getUsername());
            otp.setCode(code);

            otpDao.save(otp);
        }
    }
}

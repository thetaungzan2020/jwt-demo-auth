package com.example.authserver.contoller;

import com.example.authserver.entity.Otp;
import com.example.authserver.entity.User;
import com.example.authserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/addUser")
    public void addUser(@RequestBody User user) {
        userService.addUser(user);
    }

    @PostMapping("/auth")
    public void auth(@RequestBody User user) {
        userService.auth(user);
    }

    @PostMapping("/check")
    public void check(@RequestBody Otp otp, HttpServletResponse response) {
       if(userService.check(otp)) {
           response.setStatus(HttpServletResponse.SC_OK);
       }else {
           response.setStatus(HttpServletResponse.SC_FORBIDDEN);
       }
    }
}
